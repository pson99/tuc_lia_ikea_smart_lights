Some information about where to put the different "TUC_Lia_IKEA_smart_lights"-files
in order to use the IKEA Smart Light testautomation.


###########################################################
1. Windows TestWizard-computer
###########################################################

Files to be located under the C:\Program Files\TestWizard-folder:
---------------------------------------------------------
actions2.txt
actions3.txt
light_functions2.lua
login-sniffer.sh
stop_sniffer.sh
test_lights.sh
The content inside the "Reference_pictures" folder (.bmp-files)


Files to be located under the C:\TestWizard\Scripts folder:
----------------------------------------------------------
The content inside the "Python_light_controlling_scripts" folder shall be
located in a new folder called "Python_lights" to have it's correct path.

###########################################################
2. External Linux "sniffer" computer
###########################################################

The files "stop-wireshark.sh" as well as the "wireshark.sh" shall all
be located in /home/labatus/Skrivbord/whsniff-1.3/whsniff/1.3/-folder.

(In this case "labatus" is the user of the Ubuntu-computer)

This filepath is related to by the "actions2.txt" file located on the TestWizard-computer. 
In case you will NOT put these files in the recommended directory, you need to change the "cd commands" 
used inside the "actions2.txt" file in order to get the correct path.

Please note! The "actions2.txt" and the "actions3.txt" is both used by the scripts "login-sniffer.sh" and
"stop-sniffer.sh" that connects to the Linux-computer via plink.exe and SSH. The IP-adress to the 
Linux-computer is a part of this script and needs to be changed in case the IP-adress changes. 
At the moment the IP-adress is set to 10.46.36.145.

--------------------------------------------------------------